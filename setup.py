import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="clickup2todoist",
    version="0.0.1",
    author="Sipho Mateke",
    author_email="siphomateke@gmail.com",
    description="Command line tool to import a ClickUp CSV export to Todoist.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/siphomateke/clickup2todoist",
    packages=setuptools.find_packages(),
    license="MIT",
    install_requires=["todoist-python", "inquirer"],
    classifiers=(
        "Development Status :: 3 - Alpha",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ),
    python_requires='>=3.6',
    project_urls={
        'Bug Reports': 'https://gitlab.com/siphomateke/clickup2todoist/issues',
        'Source': 'https://gitlab.com/siphomateke/clickup2todoist',
    },
)

# TODO

- [x] Use Sync API so we can batch requests to get around rate limit
- [ ] Convert to snake_case
- [ ] Create missing labels/tags

## Data to import

- [ ] comments
- [ ] descriptions
- [ ] attachments
- [ ] subtasks
- [x] status (open/closed)
- [ ] recurring tasks
- [ ] ~~date_added~~ (Not possible)
- [ ] date_completed

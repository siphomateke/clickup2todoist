# ClickUp2Todoist

![Logo](./images/clickup2todoist-logo.png)

Command line tool to import a ClickUp CSV export to Todoist.

## Building

```bash
git clone https://gitlab.com/siphomateke/clickup2todoist.git
cd clickup2todoist
pip3 install -r requirements.txt
```

In the future installation will be as simple as:

```bash
pip3 install clickup2todoist
```

## Usage

```bash
python3 ./clickup2todoist/main.py -t "<todoist token>" -i /path/to/clickup-export.csv
```

import argparse
import csv
import json
import re
from typing import Dict, Generic, List, TypeVar
import todoist
import inquirer
from todoist.api import SyncError, TodoistAPI
from datetime import datetime


def csvArrayParse(cell) -> List[str]:
    str = re.sub("\[|\]", "", cell)
    if (not str):
        return []
    return str.split(",")


def csvNumberParse(cell) -> int:
    if (cell == "null" or cell == ""):
        return None
    return int(cell)


def csvStringParse(cell) -> str:
    if (cell == "null"):
        return ""
    return cell


class ClickUpTask():
    def __init__(self, row: Dict[str, str]) -> None:
        self.taskId = row["Task ID"]
        self.taskName = row["Task Name"]
        self.taskContent = csvStringParse(row["Task Content"])
        self.status = row["Status"]
        self.dateCreated = csvNumberParse(row["Date Created"])
        self.dateCreatedText = row["Date Created Text"]
        self.dueDate = csvNumberParse(row["Due Date"])
        self.dueDateText = row["Due Date Text"]
        self.startDate = csvNumberParse(row["Start Date"])
        self.startDateText = row["Start Date Text"]
        self.parentId = csvStringParse(row["Parent ID"])
        self.attachments = json.loads(row["Attachments"])
        self.assignees = csvArrayParse(row["Assignees"])
        self.tags = csvArrayParse(row["Tags"])
        self.priority = csvNumberParse(row["Priority"])
        self.listName = row["List Name"]
        self.projectName = row["Project Name"]
        self.spaceName = row["Space Name"]
        self.timeEstimated = row["Time Estimated"]
        self.timeEstimatedText = row["Time Estimated Text"]
        self.checklists = json.loads(row["Checklists"])
        self.comments = json.loads(row["Comments"])
        self.timeSpent = csvNumberParse(row["Time Spent"])
        self.timeSpentText = row["Time Spent Text"]
        self.rolledUpTime = int(row["Rolled Up Time"])
        self.rolledUpTimeText = row["Rolled Up Time Text"]


T = TypeVar("T")


class ClickUpHierarchyLevel(Generic[T]):
    def __init__(self, name: str, children: List[T] = None) -> None:
        self.name = name
        if children is None:
            children = []
        self.children = children


class ClickUpList(ClickUpHierarchyLevel[ClickUpTask]):
    pass


class ClickUpProject(ClickUpHierarchyLevel[ClickUpList]):
    pass


class ClickUpSpace(ClickUpHierarchyLevel[ClickUpProject]):
    pass


def parseCsv(path: str) -> List[ClickUpTask]:
    tasks: List[ClickUpTask] = []
    with open(path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            tasks.append(ClickUpTask(row))
    return tasks


def parseClickUpSpaces(tasks: List[ClickUpTask]):
    # Spaces stored by name
    spaces: Dict[str, ClickUpSpace] = {}
    for task in tasks:
        existingSpace = spaces.get(task.spaceName)
        if (existingSpace is None):
            newSpace: ClickUpSpace = ClickUpSpace(task.spaceName)
            spaces[task.spaceName] = newSpace
            existingSpace = newSpace
        existingProject = next(
            (p for p in existingSpace.children if p.name == task.projectName), False)
        if (not existingProject):
            newProject: ClickUpProject = ClickUpProject(task.projectName)
            existingSpace.children.append(newProject)
            existingProject = newProject
        existingList = next(
            (l for l in existingProject.children if l.name == task.listName), False)
        if (not existingList):
            newList: ClickUpList = ClickUpList(task.listName)
            existingProject.children.append(newList)
            existingList = newList
        existingList.children.append(task)
    return spaces


def createTask(api: TodoistAPI, parentProjectId: int, task: ClickUpTask, i: int, allLabels: List[Dict[str, any]]):
    data = {
        "project_id": parentProjectId,
        "child_order": i,  # TODO: Make sure this arg is correct
    }
    if (len(task.tags) > 0):
        api.state["labels"]
        labelIds = list(map(lambda tag: next(
            filter(lambda label: label["id"] == tag, allLabels)), task.tags))
        data["labels"] = labelIds
    # TODO: Convert dates to ISO when parsing CSV
    if (task.dueDate is not None):
        data["due"] = {
            "date": datetime.utcfromtimestamp(task.dueDate / 1000).isoformat(),
            # "is_recurring": False,
        }
    if (task.priority is not None):
        data["priority"] = task.priority
    try:
        # TODO: Don't add completed tasks, it messes with Todois'ts Karma system
        if (task.status != "Closed"):
            tTask = api.items.add(task.taskName, **data).data
        # if (task.status == "Closed"):
        #     api.items.close(tTask["id"])
    except SyncError as error:
        print("Ignored error creating task: ", error)


def createProjectRecursive(
    api: TodoistAPI,
    items: List[ClickUpHierarchyLevel],
    parentProjectId: int,
    allLabels: List[Dict[str, any]],
):
    for i, item in enumerate(items):
        if (isinstance(item, ClickUpTask)):
            createTask(api, parentProjectId, item, i, allLabels)
        else:
            createdProject = api.projects.add(
                item.name, parent_id=parentProjectId).data
            createProjectRecursive(api, item.children,
                                   createdProject["id"], allLabels)


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def main():
    parser = argparse.ArgumentParser(
        description="Command line tool to import ClickUp tasks to Todoist using a CSV export and Todoist\'s API.")
    parser.add_argument(
        "-t", "--token", help="Todoist API Token (Get from https:#todoist.com/prefs/integrations)")
    parser.add_argument(
        "-i", "--input", help="Relative path of ClickUp CSV export")
    parsed = parser.parse_args()

    clickUpTasks = parseCsv(parsed.input)
    clickUpSpaces = parseClickUpSpaces(clickUpTasks)
    print("Parsed {0} space(s) from ClickUp export CSV which contain a total of {1} task(s)".format(
        len(clickUpSpaces), len(clickUpTasks)))

    answers = inquirer.prompt([
        inquirer.Checkbox(
            "spaces",
            message="Which spaces would you like to import?",
            choices=[spaceName for spaceName in clickUpSpaces]
        )
    ])

    namesOfSpacesToImport: List[str] = answers["spaces"]
    spacesToImport = list(
        map(lambda name: clickUpSpaces[name], namesOfSpacesToImport))

    api = todoist.TodoistAPI(parsed.token)
    allLabels = [label.data for label in api.labels.all()]

    """
    # TODO: Use Listr
    tasks = Listr([
    {
        title: "Create spaces",
        task: () => {

        }
    }
    ])
    """

    allLabelNames = list(map(lambda l: l["name"], allLabels))
    print("Fetched existing labels: {0}".format(allLabelNames))

    rootTodoistProject = api.projects.add("ClickUp Import").data
    # FIXME: First sort by tasks that don't have parents so we can add subtasks
    # TODO: Multithread
    createProjectRecursive(api, spacesToImport,
                           rootTodoistProject["id"], allLabels)
    print("Committing changes")

    # Chunk to stay within request limit
    queue = api.queue
    api.queue = []
    for chunk in chunks(queue, 100):
        print("Committing {0} commands".format(len(chunk)))
        api.queue = chunk
        response = api.commit()

        if "error" in response:
            print("Error: {0} ({1})".format(
                response["error"], response["error_tag"]))

        # TODO: Make sure all sync_status values are `"ok"`


if __name__ == "__main__":
    main()

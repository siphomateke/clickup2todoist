module.exports = {
  root: true,
  env: {
    node: true,
    es6: true,
  },
  extends: [
    'airbnb',
    'plugin:@typescript-eslint/recommended',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-plusplus': 'off',
    'no-param-reassign': ['error', { 'props': false }],
    'max-len': 'off',
    'import/prefer-default-export': 'off',
    'space-infix-ops': 'off',
    'comma-spacing': 'off',
    'linebreak-style': 0,
    'prefer-destructuring': 'off'
  },
  plugins: ["@typescript-eslint"],
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
};
